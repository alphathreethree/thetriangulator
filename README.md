# README #

The Triangulator

### What is this repository for? ###

* If you don't know what it is, it is not for you.
* Beta version

### How do I get set up? ###

* Go to the app/build directory and install the apk
* You will need to enable the "install from untrusted sources" on your device
* Once you have installed the apk, I suggest immediately disabling "install from untrusted sources"
* [APK](https://bitbucket.org/alphathreethree/thetriangulator/src/78f861bf8cddd9dddf8c1267b56e2c75399f86b1/app/build/outputs/apk/?at=master)

### How do I use it? ###

* TODO: write up instructions