package com.alpha33dev.thetriangulator;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;

public class MainActivity extends AppCompatActivity {

    Button system1Button;
    Button system2Button;
    Button system3Button;
    Button system4Button;

    Button triangulate;
    Button clear;
    Button systemsDefinition;

    TextView sys1Name;
    TextView sys1X;
    TextView sys1Y;
    TextView sys1D;

    TextView sys2Name;
    TextView sys2X;
    TextView sys2Y;
    TextView sys2D;

    TextView sys3Name;
    TextView sys3X;
    TextView sys3Y;
    TextView sys3D;

    TextView sys4Name;
    TextView sys4X;
    TextView sys4Y;
    TextView sys4D;

    TextView foundCount;

    EditText unknownSystemsOutput;
    EditText knownSystemsOutput;

    StarSystem star1;
    StarSystem star2;
    StarSystem star3;
    StarSystem star4;

    SystemsXmlParser sysParser;
    LinkedList<StarSystem> systemsFromXml;

    static final int BUTTON1 = 1;
    static final int BUTTON2 = 2;
    static final int BUTTON3 = 3;
    static final int BUTTON4 = 4;
    static final int EDITSYSTEMS = 5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPref = getPreferences(MODE_PRIVATE);
        sysParser = new SystemsXmlParser();

        if (sharedPref.contains("firstrun")) // using 'firstrun' to determine if this is the first time the app has EVER been run
        {
            if (sharedPref.getBoolean("firstrun", false)) // see if firstrun is true (false is the default value
            {
                try {
                    InputStream inputStream = openFileInput("systems.xml");
                    systemsFromXml = sysParser.parse(inputStream);
                } catch (Exception ex) {
                    // file io error
                    Toast.makeText(this, "File I/O error opening internal file", Toast.LENGTH_LONG).show();
                }
            } else {
                // for some reason, the 'firstrun' name is in shared preferences, but it is set to false
                // set to true and then copy the systems.xml file
                // note: this code should NEVER be reached. if execution reaches this spot, something bad went wrong
            }

        } else {
            // this code runs when the app is run for the very first time ever. it should never execute again

            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putBoolean("firstrun", true);
            editor.commit();

            try {
                InputStream inputStream = getResources().openRawResource(R.raw.systems);
                systemsFromXml = sysParser.parse(inputStream);

                InputStream inputStream2 = getResources().openRawResource(R.raw.systems); // sloppy opening inputstream twice, should use buffered stream, but...fuck it

                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream2));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                    sb.append("\n");
                }
                String temp = sb.toString();

                FileOutputStream fos = openFileOutput("systems.xml", MODE_PRIVATE);
                fos.write(temp.getBytes());
                fos.close();
            } catch (Exception ex) {
                // file io error
                Toast.makeText(this, "File I/O error reading or writing xml file", Toast.LENGTH_LONG).show();
            }

        }

        star1 = new StarSystem(0, 0, "Unknown");
        star2 = new StarSystem(0, 0, "Unknown");
        star3 = new StarSystem(0, 0, "Unknown");
        star4 = new StarSystem(0, 0, "Unknown");

        //-----------
        // instantiate the buttons and set their "onClickListener" functions
        system1Button = (Button) findViewById(R.id.system1_button);
        system2Button = (Button) findViewById(R.id.system2_button);
        system3Button = (Button) findViewById(R.id.system3_button);
        system4Button = (Button) findViewById(R.id.system4_button);


        system1Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSystemActivity(v, BUTTON1);
            }
        });

        system2Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSystemActivity(v, BUTTON2);
            }
        });

        system3Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSystemActivity(v, BUTTON3);
            }
        });

        system4Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSystemActivity(v, BUTTON4);
            }
        });



        //----------
        // instantiate the triangulate button and set its onClickListener function
        triangulate = (Button) findViewById(R.id.triangulate_button);
        triangulate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                triangulateFunction();
            }
        });

        //----------
        // instantiate the clear button and set its onClickListener function
        clear = (Button) findViewById(R.id.clear_button);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearFunction();
            }
        });

        //----------
        // instantiate the systemsDefinition button and set its onClickListener function
        systemsDefinition = (Button) findViewById(R.id.systems_button);
        systemsDefinition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startEditSystemsActivity();
            }
        });



        //------------
        // instantiate all the text views. these are the system names and their values
        sys1Name = (TextView) findViewById(R.id.system1_name_textView);
        sys1X = (TextView) findViewById(R.id.system1_x_textView);
        sys1Y = (TextView) findViewById(R.id.system1_y_textView);
        sys1D = (TextView) findViewById(R.id.system1_d_textView);

        sys1Name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSystemActivity(v, BUTTON1);
            }
        });

        sys2Name = (TextView) findViewById(R.id.system2_name_textView);
        sys2X = (TextView) findViewById(R.id.system2_x_textView);
        sys2Y = (TextView) findViewById(R.id.system2_y_textView);
        sys2D = (TextView) findViewById(R.id.system2_d_textView);

        sys2Name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSystemActivity(v, BUTTON2);
            }
        });

        sys3Name = (TextView) findViewById(R.id.system3_name_textView);
        sys3X = (TextView) findViewById(R.id.system3_x_textView);
        sys3Y = (TextView) findViewById(R.id.system3_y_textView);
        sys3D = (TextView) findViewById(R.id.system3_d_textView);

        sys3Name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSystemActivity(v, BUTTON3);
            }
        });

        sys4Name = (TextView) findViewById(R.id.system4_name_textView);
        sys4X = (TextView) findViewById(R.id.system4_x_textView);
        sys4Y = (TextView) findViewById(R.id.system4_y_textView);
        sys4D = (TextView) findViewById(R.id.system4_d_textView);

        sys4Name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSystemActivity(v, BUTTON4);
            }
        });

        foundCount = (TextView) findViewById(R.id.found_count_textView);

        //-----------
        // instantiate the two edit text boxes that will hold the output of the triangulation
        unknownSystemsOutput = (EditText) findViewById(R.id.unknownSystems_editText);
        knownSystemsOutput = (EditText) findViewById(R.id.knownSystems_editText);

        //-----------
        // If the parent method (onCreate) was called because of an orientation change of the phone
        // there will be a bundle of saved values (set in onSaveInstanceState()). use those
        // values to re-assign to all the text boxes and such
        if (savedInstanceState != null) {
            // this will reset the text values after orientation changes
            sys1Name.setText(savedInstanceState.getString("name1"));
            sys1X.setText(savedInstanceState.getString("x1"));
            sys1Y.setText(savedInstanceState.getString("y1"));
            sys1D.setText(savedInstanceState.getString("d1"));

            sys2Name.setText(savedInstanceState.getString("name2"));
            sys2X.setText(savedInstanceState.getString("x2"));
            sys2Y.setText(savedInstanceState.getString("y2"));
            sys2D.setText(savedInstanceState.getString("d2"));

            sys3Name.setText(savedInstanceState.getString("name3"));
            sys3X.setText(savedInstanceState.getString("x3"));
            sys3Y.setText(savedInstanceState.getString("y3"));
            sys3D.setText(savedInstanceState.getString("d3"));

            sys4Name.setText(savedInstanceState.getString("name4"));
            sys4X.setText(savedInstanceState.getString("x4"));
            sys4Y.setText(savedInstanceState.getString("y4"));
            sys4D.setText(savedInstanceState.getString("d4"));

            foundCount.setText(savedInstanceState.getString("count"));

            unknownSystemsOutput.setText(savedInstanceState.getString("output"));
            knownSystemsOutput.setText(savedInstanceState.getString("output2"));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        // * This function is called when the activity is being destroyed

        // save the 12 values used on the systems
        state.putString("name1", sys1Name.getText().toString());
        state.putString("name2", sys2Name.getText().toString());
        state.putString("name3", sys3Name.getText().toString());
        state.putString("name4", sys4Name.getText().toString());

        state.putString("x1", sys1X.getText().toString());
        state.putString("x2", sys2X.getText().toString());
        state.putString("x3", sys3X.getText().toString());
        state.putString("x4", sys4X.getText().toString());

        state.putString("y1", sys1Y.getText().toString());
        state.putString("y2", sys2Y.getText().toString());
        state.putString("y3", sys3Y.getText().toString());
        state.putString("y4", sys4Y.getText().toString());

        state.putString("d1", sys1D.getText().toString());
        state.putString("d2", sys2D.getText().toString());
        state.putString("d3", sys3D.getText().toString());
        state.putString("d4", sys4D.getText().toString());

        state.putString("count", foundCount.getText().toString());

        // save the triangulate text output
        state.putString("output", unknownSystemsOutput.getText().toString());
        state.putString("output2", knownSystemsOutput.getText().toString());
    }

    private void startSystemActivity(View view, int buttonNumber) {
        // One of the buttons for the 4 systems has been clicked
        // send an "Intent" that will open up an instance of the SystemSelectActivity
        // this will be an "activity for result", meaning that the SystemSelectActivity will
        // be sending data back to this activity
        Intent systemSelectIntent = new Intent(this, SystemSelectActivity.class);
        Bundle bundle = new Bundle();
        // this is a work-around for a limitation of android:
        // LinkedList doesn't seem to want to get marshalled. gonna convert it to an arraylist first
        // see stackoverflow 12300886
        ArrayList<StarSystem> tempList = new ArrayList<>();
        for (StarSystem s : systemsFromXml) {
            tempList.add(s);
        }
        bundle.putSerializable("sysList", tempList);
        systemSelectIntent.putExtra("bundle", bundle);
        startActivityForResult(systemSelectIntent, buttonNumber); // buttonNumber tells the activity which system button was pressed
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // this is the callback function that is called when a "startActivityForResult" activity
        // returns (there are two different ones in this app)
        if (resultCode == RESULT_OK) {
            int xVal = data.getIntExtra("x", 0);
            int yVal = data.getIntExtra("y", 0);
            int dVal = data.getIntExtra("d", 0);
            String nameVal = data.getStringExtra("name");

            switch (requestCode) {
                case BUTTON1:
                    sys1X.setText(String.valueOf(xVal));
                    sys1Y.setText(String.valueOf(yVal));
                    sys1D.setText(String.valueOf(dVal));
                    sys1Name.setText(nameVal);
                    star1.setX(xVal);
                    star1.setY(yVal);
                    star1.setDistance(dVal);
                    star1.setName(nameVal);
                    break;
                case BUTTON2:
                    sys2X.setText(String.valueOf(xVal));
                    sys2Y.setText(String.valueOf(yVal));
                    sys2D.setText(String.valueOf(dVal));
                    sys2Name.setText(nameVal);
                    star2.setX(xVal);
                    star2.setY(yVal);
                    star2.setDistance(dVal);
                    star2.setName(nameVal);
                    break;
                case BUTTON3:
                    sys3X.setText(String.valueOf(xVal));
                    sys3Y.setText(String.valueOf(yVal));
                    sys3D.setText(String.valueOf(dVal));
                    sys3Name.setText(nameVal);
                    star3.setX(xVal);
                    star3.setY(yVal);
                    star3.setDistance(dVal);
                    star3.setName(nameVal);
                    break;
                case BUTTON4:
                    sys4X.setText(String.valueOf(xVal));
                    sys4Y.setText(String.valueOf(yVal));
                    sys4D.setText(String.valueOf(dVal));
                    sys4Name.setText(nameVal);
                    star4.setX(xVal);
                    star4.setY(yVal);
                    star4.setDistance(dVal);
                    star4.setName(nameVal);
                    break;
                case EDITSYSTEMS:
                    Bundle b = data.getBundleExtra("bundle");
                    ArrayList<StarSystem> tempResult = (ArrayList<StarSystem>) b.getSerializable("sysList");
                    systemsFromXml.clear();
                    for (StarSystem s : tempResult) {
                        systemsFromXml.add(s);
                    }
                    writeSystemXmlFile();
                    break;
                default:
                    break;
            }
        }

    }


    private void triangulateFunction() {
        // clear the output text boxes
        unknownSystemsOutput.setText("");
        knownSystemsOutput.setText("");

        int count = 0;  // count the number of matches/systems found

        LinkedList<StarSystem> result = calculateCandidateLocations(); // calculateCandidateLocations() is the workhorse of the app

        for (StarSystem s : result) {
            if (s.getName() == "Uncharted") {
                unknownSystemsOutput.append("(" + s.getX() + ", " + s.getY() + ")");
                unknownSystemsOutput.append("\n");
                count++;
            } else {
                knownSystemsOutput.append(s.getName() + " (" + s.getX() + ", " + s.getY() + ")");
                knownSystemsOutput.append("\n");
                count++;
            }
        }
        foundCount.setText(String.valueOf(count));      // set the textview to number of systems/matches found
    }

    private void clearFunction() {
        // This function clears all the text fields and edit text boxes
        unknownSystemsOutput.setText("");
        knownSystemsOutput.setText("");

        sys1Name.setText("Uncharted");
        sys1X.setText("0");
        sys1Y.setText("0");
        sys1D.setText("0");
        star1.setX(0);
        star1.setY(0);
        star1.setDistance(0);
        star1.setName("Unknown");

        sys2Name.setText("Uncharted");
        sys2X.setText("0");
        sys2Y.setText("0");
        sys2D.setText("0");
        star2.setX(0);
        star2.setY(0);
        star2.setDistance(0);
        star2.setName("Unknown");

        sys3Name.setText("Uncharted");
        sys3X.setText("0");
        sys3Y.setText("0");
        sys3D.setText("0");
        star3.setX(0);
        star3.setY(0);
        star3.setDistance(0);
        star3.setName("Unknown");

        sys4Name.setText("Uncharted");
        sys4X.setText("0");
        sys4Y.setText("0");
        sys4D.setText("0");
        star4.setX(0);
        star4.setY(0);
        star4.setDistance(0);
        star4.setName("Unknown");
    }

    private void startEditSystemsActivity() {
        // bundle up the systemsFromXml (starsystem) list and pass it
        // to the editSystems activity so it can populate a list view
        // it will be activity for result, the result data will be a new list
        // of systems (we will assign it to systemsFromXml)
        Intent editSystemIntent = new Intent(this, EditSystems.class);
        Bundle bundle = new Bundle();
        // this is a work-around for a limitation of android:
        // LinkedList doesn't seem to want to get marshalled. gonna convert it to an arraylist first
        // see stackoverflow 12300886
        ArrayList<StarSystem> tempList = new ArrayList<>();
        for (StarSystem s : systemsFromXml) {
            tempList.add(s);
        }
        bundle.putSerializable("sysList", tempList);
        editSystemIntent.putExtra("bundle", bundle);
        startActivityForResult(editSystemIntent, EDITSYSTEMS);
    }

    public void creditsactivity(View view){
        Intent startcreditsactivity = new Intent(this, DisplayCreditsActivity.class);
        startActivity(startcreditsactivity);
    }

    public LinkedList<StarSystem> calculateCandidateLocations()
    {
        LinkedList<StarSystem> systemList = new LinkedList();
        int found = 0;

        String nameOne = star1.getName();
        String nameTwo = star2.getName();
        String nameThree = star3.getName();
        String nameFour = star4.getName();

        ArrayList<Coordinate> coordinatesStar1;
        ArrayList<Coordinate> coordinatesStar2;
        ArrayList<Coordinate> coordinatesStar3;
        ArrayList<Coordinate> coordinatesStar4;

        // generate lists of candidate points for each star system (or custom location)
        coordinatesStar1 = generateListOfCoordinates(Integer.parseInt(sys1X.getText().toString()), Integer.parseInt(sys1Y.getText().toString()), Integer.parseInt(sys1D.getText().toString()));
        coordinatesStar2 = generateListOfCoordinates(Integer.parseInt(sys2X.getText().toString()), Integer.parseInt(sys2Y.getText().toString()), Integer.parseInt(sys2D.getText().toString()));
        coordinatesStar3 = generateListOfCoordinates(Integer.parseInt(sys3X.getText().toString()), Integer.parseInt(sys3Y.getText().toString()), Integer.parseInt(sys3D.getText().toString()));
        coordinatesStar4 = generateListOfCoordinates(Integer.parseInt(sys4X.getText().toString()), Integer.parseInt(sys4Y.getText().toString()), Integer.parseInt(sys4D.getText().toString()));

        // check to see if any of the 3 other system fields are set to some kind of value (assuming that sys1 will always have values)
        // going to assume that if the distance is 0, then that 'system' doesnt have any extra info to add
        // if distance is greater than zero, use intersects() to keep only the intersecting points
        // intersects() is home grown because I couldn't find a java native func to do intersection of lists of custom objects
        ArrayList<Coordinate> intersectionList = coordinatesStar1;

        if(Integer.parseInt(sys2D.getText().toString()) > 0)
        {
            intersectionList = intersects(coordinatesStar1, coordinatesStar2);
        }
        if(Integer.parseInt(sys3D.getText().toString()) > 0)
        {
            intersectionList = intersects(intersectionList, coordinatesStar3);
        }
        if(Integer.parseInt(sys4D.getText().toString()) > 0)
        {
            intersectionList = intersects(intersectionList, coordinatesStar4);
        }

        // now go through the list of coordinates and see if any match known system names
        // if it matches, use the name from 'systemsFromXml', if no match use "Uncharted"
        // pack each of these into 'systemList' so we can return it to the calling function
        if(intersectionList.size() > 0) {
            for (int i = 0; i < intersectionList.size(); i++) {
                String name = isKnown(intersectionList.get(i));
                if(name != null) {
                    systemList.add(new StarSystem(intersectionList.get(i).getX(), intersectionList.get(i).getY(), name));
                }
            }
        }
        else
        {
            systemList.add(new StarSystem(999, 999, "NO RESULT"));
        }

        return systemList;
    }

    public String isKnown(Coordinate s)
    {
        for (int i = 0; i < systemsFromXml.size(); i++) {
            if ((s.getX() == ((StarSystem)systemsFromXml.get(i)).getX()) && (s.getY() == ((StarSystem)systemsFromXml.get(i)).getY())) {
                return ((StarSystem)systemsFromXml.get(i)).getName();
            }
        }
        return "Uncharted";
    }

    private ArrayList<Coordinate> generateListOfCoordinates(int x, int y, int distance)
    {
        ArrayList<Coordinate> pointsList = new ArrayList();

        int startXVal = distance * -1;
        int YVal = 0;
        boolean downStroke = false; // when true means the yVal needs to start decreasing

        //
        // this for-loop creates a list of coordinate points around 0,0
        // the points will form a diamond shape around the center
        //
        // e.g. x=0, y=0, distance=3
        //
        // y:     (-1) (-2) (-3) (-2) (-1)          // these made by "new Coordinate(i, YVal * -1)"
        // y:   0   1    2    3    2    1   0
        // x:  -3  -2   -1    0    1    2   3
        //
        // that will create 12 (x,y) points around 0,0
        //
        for(int i = startXVal; i < distance + 1; i++)
        {
            pointsList.add(new Coordinate(i, YVal));
            if (YVal != 0) {
                pointsList.add(new Coordinate(i, YVal * -1));
            }

            if (downStroke) {
                YVal = YVal - 1;
            }
            else {
                YVal = YVal + 1;
            }

            if(YVal == distance)
            {
                downStroke = true;  // now start decrementing YVal
            }
        }

        //
        // now translate that generated list of points by the given x,y points
        //
        for(int j = 0; j < pointsList.size(); j++)
        {
            pointsList.get(j).setX(pointsList.get(j).getX() + x);
            pointsList.get(j).setY(pointsList.get(j).getY() + y);
        }

        return pointsList;
    }

    private ArrayList<Coordinate> intersects(ArrayList<Coordinate> left, ArrayList<Coordinate> right)
    {
        ArrayList<Coordinate> result = new ArrayList();     // create an empty list

        for(int k = 0; k < left.size(); k++)                // sucks, but need to do search that is O(n^2)
        {
            for(int m = 0; m < right.size(); m++)
            {
                if (left.get(k).equals(right.get(m)))       // 'equals' is a method from the Coordinate class
                {
                    result.add(left.get(k));                // if found a point that exists in both lists
                }                                           // add it to the result list
            }
        }

        return result;
    }

    private void writeSystemXmlFile()
    {
        StringBuilder sb = new StringBuilder();

        sb.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
        sb.append("<systems>\n");

        for(StarSystem s : systemsFromXml)
        {
            sb.append("<system>\n");
            sb.append("\t<name type=\"text\">").append(s.getName()).append("</name>\n");
            sb.append("\t<x>").append(s.getX()).append("</x>\n");
            sb.append("\t<y>").append(s.getY()).append("</y>\n");
            sb.append("</system>\n");
        }

        sb.append("</systems>\n");

        String temp = sb.toString();

        try {
            FileOutputStream fos = openFileOutput("systems.xml", MODE_PRIVATE);
            fos.write(temp.getBytes());
            fos.close();
        }
        catch(Exception ex)
        {
            // file I/O error
        }

    }

}