package com.alpha33dev.thetriangulator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class NewSystemActivity extends AppCompatActivity {

    EditText nameEditText;
    EditText xEditText;
    EditText yEditText;

    Button addButton;
    Button cancelButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_system);

        nameEditText = (EditText)findViewById(R.id.newSysetmName_editText);
        xEditText = (EditText)findViewById(R.id.newSystemX_editText);
        yEditText = (EditText)findViewById(R.id.newSystemY_editText);
        addButton = (Button)findViewById(R.id.addNewSystem_button);
        cancelButton = (Button)findViewById(R.id.cancelNewSystem_button);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addButtonClicked();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelButtonClicked();
            }
        });


        if(savedInstanceState != null)
        {
            nameEditText.setText(savedInstanceState.getString("name"));
            xEditText.setText(savedInstanceState.getString("x"));
            yEditText.setText(savedInstanceState.getString("y"));

        }

    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);

        state.putString("name", nameEditText.getText().toString());     // save all the values if user rotates phone
        state.putString("x", xEditText.getText().toString());
        state.putString("y", yEditText.getText().toString());

    }

    private void addButtonClicked()
    {
        Intent resultIntent = new Intent();
        resultIntent.putExtra("name",nameEditText.getText().toString());
        resultIntent.putExtra("x", Integer.parseInt(xEditText.getText().toString()));
        resultIntent.putExtra("y", Integer.parseInt(yEditText.getText().toString()));
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    private void cancelButtonClicked()
    {
        Intent resultIntent = new Intent();
        setResult(RESULT_CANCELED, resultIntent);
        finish();
    }


}
