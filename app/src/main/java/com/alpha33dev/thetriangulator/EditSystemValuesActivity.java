package com.alpha33dev.thetriangulator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class EditSystemValuesActivity extends AppCompatActivity {

    EditText nameEditText;
    EditText xEditText;
    EditText yEditText;

    Button saveButton;
    Button cancelButton;
    Button deleteButton;

    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_system_values);

        nameEditText = (EditText)findViewById(R.id.editSystemName_editText);
        xEditText = (EditText)findViewById(R.id.editSystemX_editText);
        yEditText = (EditText)findViewById(R.id.editSystemY_editText);
        saveButton = (Button)findViewById(R.id.editSystemSave_button);
        cancelButton = (Button)findViewById(R.id.editSystemCancel_button);
        deleteButton = (Button)findViewById(R.id.editSystemDelete_button);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveButtonClicked();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelButtonClicked();
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteButtonClicked();
            }
        });

        if(savedInstanceState == null) {
            Intent callingIntent = getIntent();
            nameEditText.setText(callingIntent.getStringExtra("name"));
            xEditText.setText(String.valueOf(callingIntent.getIntExtra("x", 0)));
            yEditText.setText(String.valueOf(callingIntent.getIntExtra("y",0)));
            position = callingIntent.getIntExtra("position", 0);
        }
        else{
            nameEditText.setText(savedInstanceState.getString("name"));
            xEditText.setText(savedInstanceState.getString("x"));
            yEditText.setText(savedInstanceState.getString("y"));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);

        state.putString("x", xEditText.getText().toString());       // save all the values if user rotates the phone
        state.putString("y", yEditText.getText().toString());
        state.putString("name", nameEditText.getText().toString());
    }

    private void saveButtonClicked()
    {
        Intent resultIntent = new Intent();
        resultIntent.putExtra("name", nameEditText.getText().toString());
        resultIntent.putExtra("x", Integer.parseInt(xEditText.getText().toString()));
        resultIntent.putExtra("y", Integer.parseInt(yEditText.getText().toString()));
        resultIntent.putExtra("position", position);
        resultIntent.putExtra("delete", false);
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    private void cancelButtonClicked()
    {
        Intent resultIntent = new Intent();
        setResult(RESULT_CANCELED, resultIntent);
        finish();
    }

    private void deleteButtonClicked()
    {
        // TODO: Need a confirmation dialog here
        Intent resultIntent = new Intent();
        resultIntent.putExtra("position", position);
        resultIntent.putExtra("delete", true);
        setResult(RESULT_OK, resultIntent);
        finish();
    }


}
