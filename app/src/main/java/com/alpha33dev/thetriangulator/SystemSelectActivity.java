package com.alpha33dev.thetriangulator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.LinkedList;

public class SystemSelectActivity extends AppCompatActivity {

    Button okButton;

    EditText xText;
    EditText yText;
    EditText distanceText;

    int xVal;
    int yVal;
    int dVal;
    String nameVal;

    Spinner spinner;

    ArrayList<StarSystem> systemList;

    Intent callingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_system_select);

        nameVal = "Uncharted";

        xText = (EditText)findViewById(R.id.x_editText);
        yText = (EditText)findViewById(R.id.y_editText);
        distanceText = (EditText)findViewById(R.id.distance_editText);

        if(savedInstanceState != null)
        {
            xText.setText(savedInstanceState.getString("xv"));
            yText.setText(savedInstanceState.getString("yv"));
            distanceText.setText(savedInstanceState.getString("dis"));
            nameVal = savedInstanceState.getString("name");
        }

        // setting up the drop down list of systems
        callingIntent = getIntent();
        Bundle b = callingIntent.getBundleExtra("bundle");
        systemList = (ArrayList<StarSystem>)b.getSerializable("sysList");

        spinner = (Spinner)findViewById(R.id.spinner);
        ArrayAdapter<StarSystem> adapter = new ArrayAdapter<StarSystem>(this, android.R.layout.simple_spinner_item, systemList);

        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                StarSystem temp = (StarSystem) spinner.getSelectedItem();
                xText.setText(String.valueOf(temp.getX()));
                yText.setText(String.valueOf(temp.getY()));
                nameVal = temp.getName();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        okButton = (Button)findViewById(R.id.ok_button);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnValues();
            }
        });


    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);

        state.putString("xv", xText.getText().toString());
        state.putString("yv", yText.getText().toString());
        state.putString("dis", distanceText.getText().toString());
        state.putString("name", nameVal);
    }


    private void returnValues()
    {
        xVal = Integer.parseInt(xText.getText().toString());
        yVal = Integer.parseInt(yText.getText().toString());
        dVal = Integer.parseInt(distanceText.getText().toString());

        Intent resultIntent = new Intent();
        resultIntent.putExtra("x", xVal);
        resultIntent.putExtra("y", yVal);
        resultIntent.putExtra("d", dVal);
        resultIntent.putExtra("name", nameVal);
        setResult(RESULT_OK, resultIntent);
        finish();
    }
}
