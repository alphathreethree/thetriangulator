package com.alpha33dev.thetriangulator;

public class Coordinate
{

    int x;
    int y;

    public Coordinate(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public int getX()
    {
        return this.x;
    }
    public void setX(int x)
    {
        this.x = x;
    }

    public int getY()
    {
        return this.y;
    }

    public void setY(int y)
    {
        this.y = y;
    }

    public boolean equals(Coordinate c)
    {
        if(this.x == c.getX() && this.y == c.getY())
        {
            return true;       // equal
        }
        else
        {
            return false;   // greater than
        }
    }

}
