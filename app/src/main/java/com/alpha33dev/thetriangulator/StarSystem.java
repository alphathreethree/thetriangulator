package com.alpha33dev.thetriangulator;

import java.io.Serializable;

public class StarSystem implements Serializable
{
    public int xLocation;
    public int yLocation;
    public int distanceToTarget;
    public String nameOfSystem;

    public StarSystem(int x, int y, String name)
    {
        xLocation = x;
        yLocation = y;
        nameOfSystem = name;
        distanceToTarget = 0;
    }

    public void setName(String s)
    {
        nameOfSystem = s;
    }

    @Override
    public String toString()
    {
        return nameOfSystem;
    }

    public String getName()
    {
        return nameOfSystem;
    }

    public int getX()
    {
        return xLocation;
    }

    public void setX(int x)
    {
        xLocation = x;
    }

    public int getY()
    {
        return yLocation;
    }

    public void setY(int y)
    {
        yLocation = y;
    }

    public int getDistance()
    {
        return distanceToTarget;
    }

    public void setDistance(int distance)
    {
        distanceToTarget = distance;
    }

    public boolean compareDistance(int a, int b)
    {
        if(Math.abs(getX() - a) + Math.abs(getY() - b) == getDistance())
        {
            return true;
        }
        return false;
    }


}
