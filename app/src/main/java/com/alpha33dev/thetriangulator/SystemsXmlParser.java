package com.alpha33dev.thetriangulator;


import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class SystemsXmlParser
{
    public SystemsXmlParser()
    {
        //constructor
    }

    public LinkedList<StarSystem> parse(InputStream in) throws XmlPullParserException, IOException
    {
        XmlPullParser parser = Xml.newPullParser();
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        parser.setInput(in, null);
        parser.nextTag();
        return readFeed(parser);
    }

    private LinkedList<StarSystem> readFeed(XmlPullParser parser) throws XmlPullParserException, IOException
    {
        LinkedList<StarSystem> systems = new LinkedList<>();

        parser.require(XmlPullParser.START_TAG, null, "systems");
        while(parser.next() != XmlPullParser.END_TAG)
        {
            if(parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }
            String name = parser.getName();
            if(name.equals("system"))
            {
                systems.add(readSystem(parser));
            }
            else
            {
                skip(parser);
            }
        }
        return systems; // List
    }


    private StarSystem readSystem(XmlPullParser parser) throws XmlPullParserException, IOException
    {
        parser.require(XmlPullParser.START_TAG, null, "system");
        String name = "Uncharted";
        int x = 999;
        int y = 999;

        while(parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }
            String tag = parser.getName();
            if(tag.equals("name")) {
                name = readName(parser);
            } else if(tag.equals("x"))
            {
                x = readX(parser);
            } else if(tag.equals("y"))
            {
                y = readY(parser);
            } else {
                skip(parser);
            }
        }
        return new StarSystem(x, y, name);
    }

    private String readName(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        parser.require(XmlPullParser.START_TAG, null, "name");
        String name = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, "name");
        return name;
    }

    private int readX(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        int x = 0;
        parser.require(XmlPullParser.START_TAG, null, "x");
        String xString = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, "x");
        return Integer.parseInt(xString);
    }

    private int readY(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        int y = 0;
        parser.require(XmlPullParser.START_TAG, null, "y");
        String yString = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, "y");
        return Integer.parseInt(yString);
    }

    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        String result = "";
        if(parser.next() == XmlPullParser.TEXT)
        {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException
    {
        if(parser.getEventType() != XmlPullParser.START_TAG)
        {
            throw new IllegalStateException();
        }
        int depth = 1;
        while(depth != 0)
        {
            switch(parser.next())
            {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }

}
