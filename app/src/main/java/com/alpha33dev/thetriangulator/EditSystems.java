package com.alpha33dev.thetriangulator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class EditSystems extends AppCompatActivity {

    Button addNewSystemButton;
    ListView systemsListView;
    Intent callingIntent;
    ArrayList<StarSystem> systemList;
    ArrayAdapter<StarSystem> adapter;

    static final int SYSTEMEDIT = 1;
    static final int SYSTEMNEW = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_systems);

        addNewSystemButton = (Button)findViewById(R.id.addNewSystem_button);
        systemsListView = (ListView)findViewById(R.id.editSystems_listView);

        callingIntent = getIntent();
        Bundle b = callingIntent.getBundleExtra("bundle");
        systemList = (ArrayList<StarSystem>)b.getSerializable("sysList");
        adapter = new ArrayAdapter<StarSystem>(this, android.R.layout.simple_list_item_1, systemList);
        systemsListView.setAdapter(adapter);
        systemsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startEditingActivity(position);
            }
        });

        addNewSystemButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNewSystemActivity();
            }
        });

    }

    private void startEditingActivity(int position)
    {
        Intent editSystemIntent = new Intent(this, EditSystemValuesActivity.class);
        editSystemIntent.putExtra("name", systemList.get(position).getName());
        editSystemIntent.putExtra("x", systemList.get(position).getX());
        editSystemIntent.putExtra("y", systemList.get(position).getY());
        editSystemIntent.putExtra("position", position);
        startActivityForResult(editSystemIntent, SYSTEMEDIT);
    }

    private void startNewSystemActivity()
    {
        Intent newSystemIntent = new Intent(this, NewSystemActivity.class);
        startActivityForResult(newSystemIntent, SYSTEMNEW);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK) {
            switch (requestCode) {
                case SYSTEMEDIT:
                    // if the Delete button was pressed on the EditSystemValuesActivity, it will
                    // return an intent with the "delete" value set to true
                    boolean delete = data.getBooleanExtra("delete", false);
                    int pos = data.getIntExtra("position", 0);

                    if(delete)
                    {
                        systemList.remove(pos); // remove the StarSystem at this position
                    }
                    else {
                        systemList.get(pos).setName(data.getStringExtra("name"));
                        systemList.get(pos).setX(data.getIntExtra("x", 0));
                        systemList.get(pos).setY(data.getIntExtra("y", 0));
                    }
                    adapter.notifyDataSetChanged();                         // this will refresh the listview
                    break;
                case SYSTEMNEW:
                    String tempName = data.getStringExtra("name");
                    int tempX = data.getIntExtra("x", 0);
                    int tempY = data.getIntExtra("y", 0);
                    StarSystem temp = new StarSystem(tempX, tempY, tempName);
                    systemList.add(temp);
                    adapter.notifyDataSetChanged();                         // this will refresh the listview
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void onBackPressed()
    {
        // this is called when user pressed the physical back button on the phone
        // this will be used to return an Intent with a new list of StarSystems to the MainActivity

        Intent resultIntent = new Intent();
        Bundle bundle = new Bundle();
        ArrayList<StarSystem> tempList = new ArrayList<>();
        for(StarSystem s : systemList)
        {
            tempList.add(s);
        }
        bundle.putSerializable("sysList", tempList);
        resultIntent.putExtra("bundle", bundle);
        setResult(RESULT_OK, resultIntent);
        finish();
    }

}
